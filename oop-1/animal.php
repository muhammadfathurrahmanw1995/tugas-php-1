<?php

Class animal {
    public $nama;
    public $jumlah_legs = 4;
    public $blooded = 'no';

    public function __construct($nama)
    {
        $this->nama = $nama;
    }

    public function get_nama()
    {
        return $this->nama;
    }
    public function get_jumlah_legs()
    {
        return$this->jumlah_legs; 
    }
    public function get_blooded()
    {
        return $this->blooded;
    }
}
//$san = new shaun("shaun");
//echo "Name : ". $san->get_nama()."<br>";
//echo "legs : ".$san->get_jumlah_legs(). "<br>";
//echo "cold blooded : ".$san->get_blooded(). "<br>";