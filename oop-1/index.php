<?php
require('animal.php');
require('frog.php');
require('ape.php');

$san = new animal("shaun");
echo "Name : ". $san->get_nama()."<br>";
echo "legs : ".$san->get_jumlah_legs(). "<br>";
echo "cold blooded : ".$san->get_blooded(). "<br>";

echo "<br>";
$bdk = new frog("buduk");
echo "Name : ". $bdk->get_nama()."<br>";
echo "legs : ".$bdk->get_jumlah_legs(). "<br>";
echo "cold blooded : ".$bdk->get_blooded(). "<br>";
echo "jump : ".$bdk->jump();
echo "<br>";
echo "<br>";

$ksi = new ape("kera sakti");
echo "Name : ". $ksi->get_nama()."<br>";
echo "legs : ".$ksi->get_jumlah_legs(). "<br>";
echo "cold blooded : ".$ksi->get_blooded(). "<br>";
echo "yell : ".$ksi->yell();

?>